﻿/*
 * Created by Ranorex
 * User: amorales
 * Date: 4/24/2018
 * Time: 3:26 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using System.ServiceProcess;
using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;

namespace Intellitrak_UI_Tests
{
    /// <summary>
    /// Ranorex user code collection. A collection is used to publish user code methods to the user code library.
    /// </summary>
    [UserCodeCollection]
    public class GSLCommonTestSteps
    {
    	//TimeSpan(hours,minutes,seconds)
        private static TimeSpan timeoutDefault = new TimeSpan(0,0,30);
        
        // You can use the "Insert New User Code Method" functionality from the context menu,
        // to add a new method with the attribute [UserCodeMethod].
        
        [UserCodeMethod]
         public static void StartWindowsServices()
        {

            StartWindowServiceByName("LocationServer", timeoutDefault);
            StartWindowServiceByName("StateEngineHost", timeoutDefault);
            StartWindowServiceByName("ApplicationServer", timeoutDefault);
            StartWindowServiceByName("GslCollectorService", timeoutDefault);
            StartWindowServiceByName("SecurityTokenService", timeoutDefault);
        }
         
        private static void StartWindowServiceByName(string serviceName, TimeSpan timeout)
        {
            ServiceController service = new ServiceController(serviceName);
            try
            {
                if ((service.Status.Equals(ServiceControllerStatus.Stopped)) || (service.Status.Equals(ServiceControllerStatus.StopPending)))
                {
                    service.Start();
                    service.WaitForStatus(ServiceControllerStatus.Running, timeout);
                }
            }
            catch(System.ServiceProcess.TimeoutException e)
            {
               
            }
            finally
            {
                service.Dispose();
            }

        }
        
        [UserCodeMethod]
        public static void StopWindowsServices()
        {
            StopWindowServiceByName("LocationServer", timeoutDefault);
            StopWindowServiceByName("StateEngineHost", timeoutDefault);
            StopWindowServiceByName("ApplicationServer", timeoutDefault);
            StopWindowServiceByName("GslCollectorService", timeoutDefault);
            StopWindowServiceByName("SecurityTokenService", timeoutDefault);

        }

        private static void StopWindowServiceByName(string serviceName,TimeSpan timeout)
        {
            ServiceController service = new ServiceController(serviceName);
            try
            {
                if ((service.Status.Equals(ServiceControllerStatus.Running)) || (service.Status.Equals(ServiceControllerStatus.StartPending)))
                {
                    service.Stop();
                    service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);
                }

            }
            catch (System.ServiceProcess.TimeoutException e)
            {
                
            }
            finally
            {
                service.Dispose();
            }
        }

		[UserCodeMethod]
        public static void RestartServices()
        {
            StopWindowServiceByName("LocationServer", timeoutDefault);
            StopWindowServiceByName("StateEngineHost", timeoutDefault);
            StopWindowServiceByName("ApplicationServer", timeoutDefault);
            StopWindowServiceByName("GslCollectorService", timeoutDefault);
            StopWindowServiceByName("SecurityTokenService", timeoutDefault);
            StartWindowServiceByName("LocationServer", timeoutDefault);
            StartWindowServiceByName("StateEngineHost", timeoutDefault);
            StartWindowServiceByName("ApplicationServer", timeoutDefault);
            StartWindowServiceByName("GslCollectorService", timeoutDefault);
            StartWindowServiceByName("SecurityTokenService", timeoutDefault);
        }

        private static void RestartServiceByName(string serviceName, TimeSpan timeout)
        {
            ServiceController service = new ServiceController(serviceName);
            try
            {
                service.Stop();
                service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);

                service.Start();
                service.WaitForStatus(ServiceControllerStatus.Running, timeout);
            }
            catch
            {
                // ...
            }
            finally
            {
                service.Close();
            }
        }
    }
}