﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace Intellitrak_UI_Tests
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The ImportConfigurationSettings recording.
    /// </summary>
    [TestModule("9aa6493c-7aa4-486b-b182-ebdfb4f5283a", ModuleType.Recording, 1)]
    public partial class ImportConfigurationSettings : ITestModule
    {
        /// <summary>
        /// Holds an instance of the Intellitrak_UI_TestsRepository repository.
        /// </summary>
        public static Intellitrak_UI_TestsRepository repo = Intellitrak_UI_TestsRepository.Instance;

        static ImportConfigurationSettings instance = new ImportConfigurationSettings();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public ImportConfigurationSettings()
        {
            importFileFullPath = "C:\\GSL\\ui_automation\\Ranorex\\Intellitrak_UI_Tests\\TestCommons_for_UI\\Settings_2.0.0240.0.cfgex";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static ImportConfigurationSettings Instance
        {
            get { return instance; }
        }

#region Variables

        string _importFileFullPath;

        /// <summary>
        /// Gets or sets the value of variable importFileFullPath.
        /// </summary>
        [TestVariable("7a6e1d1d-2284-43fb-8b96-178a983e6c12")]
        public string importFileFullPath
        {
            get { return _importFileFullPath; }
            set { _importFileFullPath = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", "8.0")]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", "8.0")]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 0;
            Keyboard.DefaultKeyPressTime = 20;
            Delay.SpeedFactor = 0.00;

            Init();

            Report.Log(ReportLevel.Info, "Application", "Run application 'C:\\Program Files (x86)\\GSL Solutions\\GSL Will Call Server\\V01.00.00\\Configurator.exe' with arguments '' in normal mode.", new RecordItemIndex(0));
            Host.Local.RunApplication("C:\\Program Files (x86)\\GSL Solutions\\GSL Will Call Server\\V01.00.00\\Configurator.exe", "", "C:\\Program Files (x86)\\GSL Solutions\\GSL Will Call Server\\V01.00.00", false);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Right Click item 'ConfiguratorForm.ApplicationServer' at 55;5.", repo.ConfiguratorForm.ApplicationServerInfo, new RecordItemIndex(1));
            repo.ConfiguratorForm.ApplicationServer.Click(System.Windows.Forms.MouseButtons.Right, "55;5");
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Configurator.Settings' at 40;9.", repo.Configurator.SettingsInfo, new RecordItemIndex(2));
            repo.Configurator.Settings.Click("40;9");
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Open.Text1148' at 443;9.", repo.Open.Text1148Info, new RecordItemIndex(3));
            repo.Open.Text1148.Click("443;9");
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence from variable '$importFileFullPath'.", new RecordItemIndex(4));
            Keyboard.Press(importFileFullPath);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Open.ButtonOpen' at 37;3.", repo.Open.ButtonOpenInfo, new RecordItemIndex(5));
            repo.Open.ButtonOpen.Click("37;3");
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'ImportSettingsDialog.Outline' at 27;10.", repo.ImportSettingsDialog.OutlineInfo, new RecordItemIndex(6));
            repo.ImportSettingsDialog.Outline.Click("27;10");
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'ImportSettingsDialog.Outline' at 27;184.", repo.ImportSettingsDialog.OutlineInfo, new RecordItemIndex(7));
            repo.ImportSettingsDialog.Outline.Click("27;184");
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'ImportSettingsDialog.OKButton' at 25;9.", repo.ImportSettingsDialog.OKButtonInfo, new RecordItemIndex(8));
            repo.ImportSettingsDialog.OKButton.Click("25;9");
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'ConfiguratorForm.CloseButton' at 40;7.", repo.ConfiguratorForm.CloseButtonInfo, new RecordItemIndex(9));
            repo.ConfiguratorForm.CloseButton.Click("40;7");
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
