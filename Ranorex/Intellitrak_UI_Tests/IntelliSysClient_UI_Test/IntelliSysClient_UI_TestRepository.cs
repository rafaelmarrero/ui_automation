﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Repository;
using Ranorex.Core.Testing;

namespace IntelliSysClient_UI_Test
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    /// The class representing the IntelliSysClient_UI_TestRepository element repository.
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCode("Ranorex", "8.0")]
    [RepositoryFolder("2b89e703-77e2-49c0-9131-c7bab181fbf7")]
    public partial class IntelliSysClient_UI_TestRepository : RepoGenBaseFolder
    {
        static IntelliSysClient_UI_TestRepository instance = new IntelliSysClient_UI_TestRepository();

        /// <summary>
        /// Gets the singleton class instance representing the IntelliSysClient_UI_TestRepository element repository.
        /// </summary>
        [RepositoryFolder("2b89e703-77e2-49c0-9131-c7bab181fbf7")]
        public static IntelliSysClient_UI_TestRepository Instance
        {
            get { return instance; }
        }

        /// <summary>
        /// Repository class constructor.
        /// </summary>
        public IntelliSysClient_UI_TestRepository() 
            : base("IntelliSysClient_UI_TestRepository", "/", null, 0, false, "2b89e703-77e2-49c0-9131-c7bab181fbf7", ".\\RepositoryImages\\IntelliSysClient_UI_TestRepository2b89e703.rximgres")
        {
        }

#region Variables

#endregion

        /// <summary>
        /// The Self item info.
        /// </summary>
        [RepositoryItemInfo("2b89e703-77e2-49c0-9131-c7bab181fbf7")]
        public virtual RepoItemInfo SelfInfo
        {
            get
            {
                return _selfInfo;
            }
        }
    }

    /// <summary>
    /// Inner folder classes.
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCode("Ranorex", "8.0")]
    public partial class IntelliSysClient_UI_TestRepositoryFolders
    {
    }
#pragma warning restore 0436
}