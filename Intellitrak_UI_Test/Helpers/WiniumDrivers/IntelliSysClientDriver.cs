﻿using System;
using System.Collections.ObjectModel;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Winium;

namespace IntelliSysClient_UITest.Commons
{
	class IntelliSysClientDriver : IWebDriver
	{

		#region TestClass properties and constants
		//Add TesClass Properties and/or constants here
		private string sutFullPath;
		private string winiumDriverPath;
		private DesktopOptions sutOptions;
		private WiniumDriver driver;
		#endregion

		#region IWebDriver implementations
		public string Url
		{
			get
			{
				return ((IWebDriver) driver).Url;
			}

			set
			{
				((IWebDriver) driver).Url = value;
			}
		}

		public string Title
		{
			get
			{
				return ((IWebDriver) driver).Title;
			}
		}

		public string PageSource
		{
			get
			{
				return ((IWebDriver) driver).PageSource;
			}
		}

		public string CurrentWindowHandle
		{
			get
			{
				return ((IWebDriver) driver).CurrentWindowHandle;
			}
		}

		public ReadOnlyCollection<string> WindowHandles
		{
			get
			{
				return ((IWebDriver) driver).WindowHandles;
			}
		}

		#endregion

		#region Constructor(s)

		public IntelliSysClientDriver(string newPath = @"C:\Program Files (x86)\GSL Solutions\GSL IntelliSys Client\IntelliSysClient.application")
		{
			sutOptions = new DesktopOptions();
			sutFullPath = newPath;
			//sutOptions.LaunchDelay = 1000;
			sutOptions.DebugConnectToRunningApp = false;
			sutOptions.ApplicationPath = sutFullPath;
			driver = new WiniumDriver(@"C:\GSL\ui_automation\Intellitrak_UI_Test\Helpers\WiniumDrivers", sutOptions);
			Thread.Sleep(3000);
		}

		internal object FindElement(By by)
		{
			throw new NotImplementedException(@"Could not find element "+by.ToString());
		}

		public void Close()
		{
			((IWebDriver) driver).Close();
		}

		public void Quit()
		{
			((IWebDriver) driver).Quit();
		}

		public IOptions Manage()
		{
			return ((IWebDriver) driver).Manage();
		}

		public INavigation Navigate()
		{
			return ((IWebDriver) driver).Navigate();
		}

		public ITargetLocator SwitchTo()
		{
			return ((IWebDriver) driver).SwitchTo();
		}

		IWebElement ISearchContext.FindElement(By by)
		{
			return ((IWebDriver) driver).FindElement(by);
		}

		public ReadOnlyCollection<IWebElement> FindElements(By by)
		{
			return ((IWebDriver) driver).FindElements(by);
		}

		public void Dispose()
		{
			((IWebDriver) driver).Dispose();
		}
		#endregion


	}
}
