﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace IntelliSysClient_UITest.Pages
{
	class ScanIDBadgePage
	{
		private IWebDriver driver;

		public ScanIDBadgePage(IWebDriver driver)
		{
			this.driver = driver;
			PageFactory.InitElements(driver, this);
		}

		[FindsBy(How = How.Id, Using = "Close")]
		private IWebElement btnClose;

		[FindsBy(How = How.Id, Using = "LoginWin")]
		private IWebElement scanWin;

		[FindsBy(How = How.Id, Using = "")]
		private IWebElement lblIntelliSysVersion;

		[FindsBy(How = How.Id, Using = "btnUnableToScanYourIDBadge")]
		private IWebElement btnUnableToScanYourIDBadge;

		public void ClickAtScanIDBadgeWindow()
		{
			scanWin.Click();
		}

		public void ClickUnableToScanYourIDBadge()
		{
			btnUnableToScanYourIDBadge.Click();
		}

        public string GetIntelliSysClientVersion()
		{
            return lblIntelliSysVersion.GetAttribute("");
		}

		public void Close()
		{
			btnClose.Click();
		}
	}
}
