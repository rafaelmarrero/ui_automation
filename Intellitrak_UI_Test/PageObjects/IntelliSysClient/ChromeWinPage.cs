﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IntelliSysClient_UITest.Commons;
using OpenQA.Selenium;
using OpenQA.Selenium.Winium;

namespace IntelliSysClient_UITest.Pages
{
	class ChromeWindowPage
	{
        #region Page Properties

        //chrome header
        private IWebElement winChrome;
        private IWebElement btnClose;
        private IWebElement txtActiveNavigationCategory;
        private IWebElement txtCurrentOperation;
        private IWebElement txtcurrentUserFriendlyName;
		private IWebElement txtcurrentUserIDBadge;
        private IWebElement txtTitle;

        //Shutdown Mode Popup Control
        private IWebElement ctrShutdownModePopup;
        private IWebElement lblShutdownTimer;
        private IWebElement lblShutdownDate;
        private IWebElement txtShutdownModeInstructions;
        private IWebElement btnNavigateToBackupFromShutdownModePopup;
        private IWebElement btnSnoozeFromShutdownModePopup;



        #endregion


        public ChromeWindowPage(IWebDriver driver)
		{
            winChrome = driver.FindElement(By.Id("ChromeWin"));
            btnClose = driver.FindElement(By.Id("Close"));
            txtActiveNavigationCategory = driver.FindElement(By.Id("txtActiveNavigationCategory"));
            txtCurrentOperation = driver.FindElement(By.Id("txtCurrentOperation"));
            txtcurrentUserFriendlyName = driver.FindElement(By.Id("txtcurrentUserFriendlyName"));
            txtcurrentUserIDBadge = driver.FindElement(By.Id("txtcurrentUserIDBadge"));
            txtTitle = driver.FindElement(By.Id("txtTitle"));
        }

        public void Close()
        {
            btnClose.Click();
        }

        public bool IsChromeWindowVisible()
        {
            return winChrome.Displayed;
        }

		public string GetCurrentUserFriendlyName()
		{
			return txtcurrentUserFriendlyName.Text;
		}

		public string GetCurrentUserIdBadge()
		{
			return txtcurrentUserIDBadge.Text;
		}

        public string GetMainContentTittle()
        {
            return txtTitle.Text;
        }

        public string GetCurrentNavigationCategory()
        {
            return txtActiveNavigationCategory.Text;
        }

        public string GetCurrentOperation()
        {
            return txtCurrentOperation.Text;
        }

	}
}

