﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace IntelliSysClient_UITest.Pages
{
	class LoginPage
	{
		private IWebDriver driver;

		public LoginPage(IWebDriver driver)
		{
			this.driver = driver;
			PageFactory.InitElements(driver, this);
		}

		[FindsBy(How = How.Name, Using = "Close")]
		private IWebElement btnClose;

		[FindsBy(How = How.Id, Using = "LoginWin")]
		private IWebElement loginWin;

		[FindsBy(How = How.Id, Using = "")]
		private IWebElement lblIntelliSysVersion;

		[FindsBy(How = How.Id, Using = "pbTagID")]
		private IWebElement pbTagID;

		[FindsBy(How = How.Id, Using = "txtLoginName")]
		private IWebElement txtLoginName;

		[FindsBy(How = How.Id, Using = "pbPassword")]
		private IWebElement pbPassword;

		[FindsBy(How = How.Id, Using = "btnLogin")]
		private IWebElement btnLogin;

		[FindsBy(How = How.Id, Using = "btnReturnToScanIDBadge")]
		private IWebElement btnReturnToScanIDBadge;

		[FindsBy(How = How.Id, Using = "lblErrorMessageLogin")]
		private IWebElement lblErrorMessageLogin;

		public string GetlblErrorMessageLogin()
		{
			return lblErrorMessageLogin.GetAttribute("text");
		}

		public void SetIDBadge(string ID)
		{
			pbTagID.SendKeys(ID);
		}

		public void SetLoginName(string loginName)
		{
			txtLoginName.SendKeys(loginName);
		}

		public void SetPassword(string password)
		{
			pbPassword.SendKeys(password);
		}

		public void ClickReturnToScanYourIDBadgeButton()
		{
			btnReturnToScanIDBadge.Click();
		}

		public bool IsLoginButtonDisabled()
		{
			return ! btnLogin.Enabled;
		}

		public bool IsLoginButtonEnabled()
		{
			return btnLogin.Enabled;
		}

		public void ClickLoginButton()
		{
			btnLogin.Click();
		}

		public void ClickAtLoginWin()
		{
			loginWin.Click();
		}

		public void ClickAtCloseButton()
		{
			btnClose.Click();
		}

		public void EnterLoginCredentials(string idBadge, string userName, string password)
		{
			pbTagID.Clear();
			pbTagID.SendKeys(idBadge);
			txtLoginName.Clear();
			txtLoginName.SendKeys(userName);
			pbPassword.Clear();
			pbPassword.SendKeys(password);
		}
	}
}
