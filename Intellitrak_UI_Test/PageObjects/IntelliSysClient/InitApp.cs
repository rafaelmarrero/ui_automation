﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace IntelliSysClient_UITest.Pages
{
	class InitAppPage
	{
        //Window
        private IWebElement initAppWin;
        //labels
        private IWebElement lblInitApp;
		private IWebElement lblInitAppVersion;
		//buttons
		private IWebElement btnInitAppClose;
		private IWebElement btnInitAppMoreInfo;
		private IWebElement btnInitAppBack;

        public InitAppPage(IWebDriver driver)
		{
			btnInitAppClose = driver.FindElement(By.Id("btnInitAppClose"));
			initAppWin = driver.FindElement(By.Id(""));
			btnInitAppMoreInfo = driver.FindElement(By.Id("btnToggleView"));
			btnInitAppBack = driver.FindElement(By.Id("btnToggleView"));
        }

		public void ClickAtMoreInfoButton()
		{
			btnInitAppMoreInfo.Click();
		}

		public void ClickAtBackButton()
		{
			btnInitAppBack.Click();
		}

		public void ClickAtClose()
		{
			btnInitAppClose.Click();
		}
	}
}
