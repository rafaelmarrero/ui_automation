﻿using NUnit.Framework;
using IntelliSysClient_UITest.Commons;
using IntelliSysClient_UITest.Pages;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System.Diagnostics;

namespace IntelliSysClient_UITest
{
	/// <summary>
	/// Add a description of what this class is testing against
	/// </summary>
	/// 
	[TestFixture]
	public class LoginTests
	{
		#region TestClass properties and constants
		//Add TesClass Properties and/or constants here
		//private IntelliSysClientDriver sutDriver;
		private IWebDriver sutDriver;
		private ScanIDBadgePage scanIDBadgePage;
		LoginPage loginPage;
		#endregion

		#region Setup and Tear down
		/// <summary>
		/// This runs only once at the beginning of all tests and is used for all tests in the 
		/// class.
		/// </summary>
		[OneTimeSetUp]
		public void InitialSetup()
		{
			sutDriver = new IntelliSysClientDriver();
		}

		/// <summary>
		/// This runs only once at the end of all tests and is used for all tests in the class.
		/// </summary>
		[OneTimeTearDown]
		public void FinalTearDown()
		{
        }

		/// <summary>
		/// This setup funcitons runs before each test method 
		/// </summary>
		[SetUp]
		public void SetupForEachTest()
		{
		}

		/// <summary>
		/// This setup funcitons runs after each test method
		/// </summary>
		[TearDown]
		public void TearDownForEachTest()
		{
			sutDriver.FindElement(By.Name("Close"));
		}
		#endregion

		[Test]
		[Category("UI")]
        [Order(1)]
		public void CheckLoginButtonDisabled()
		{
			#region Step 1 - Arrange
			#endregion

			#region Step 2 - Act
			scanIDBadgePage = new ScanIDBadgePage(sutDriver);
			scanIDBadgePage.ClickAtScanIDBadgeWindow();
			scanIDBadgePage.ClickUnableToScanYourIDBadge();

			loginPage = new LoginPage(sutDriver);
			PageFactory.InitElements(sutDriver, loginPage);
			#endregion

			#region Step 3 - Assert
			Assert.True(loginPage.IsLoginButtonDisabled(), "Login button is not disabled as expected.");
			#endregion
		}

		[Test]
		[Category("UI")]
		[Order(2)]
		public void CheckLoginButtonEnabled()
		{
			#region Step 1 - Arrange
			string id = "2";
			string login = "marrerotech1";
			string domain = "@local";
			string pass = "1q@W3e$R";
			#endregion

			#region Step 2 - Act
			ScanIDBadgePage scanIDBadgePage = new ScanIDBadgePage(sutDriver);
			scanIDBadgePage.ClickAtScanIDBadgeWindow();
			scanIDBadgePage.ClickUnableToScanYourIDBadge();

			LoginPage loginPage = new LoginPage(sutDriver);
			PageFactory.InitElements(sutDriver, loginPage);
			loginPage.SetIDBadge(id);
			#endregion

			#region Step 3 - Assert
			Assert.True(loginPage.IsLoginButtonEnabled(), "Login button is not enabled as expected.");
			#endregion
		}

		[Test]
        [Category("UI")]
        [Order(3)]
        public void CheckLoginErrorMessage()
        {

			#region Step 1 - Arrange
			string id = "1";
            string login = "NoUser";
            string errorMessage = "Login Unsuccessful. Please try again.";
            string domain = "@local";
            string pass = "1q@W3e$R";
			#endregion

			#region Step 2 - Act
			ScanIDBadgePage scanIDBadgePage = new ScanIDBadgePage(sutDriver);
			scanIDBadgePage.ClickAtScanIDBadgeWindow();
			scanIDBadgePage.ClickUnableToScanYourIDBadge();

			LoginPage loginPage = new LoginPage(sutDriver);
			PageFactory.InitElements(sutDriver, loginPage);
			loginPage.EnterLoginCredentials(id, login, pass);
			loginPage.ClickLoginButton();
			#endregion

			#region Step 3 - Assert
			//Specify the pass criteria for the test, which fails it if not met 
			Assert.AreEqual(errorMessage, loginPage.GetlblErrorMessageLogin());
			#endregion
		}

	}
}
